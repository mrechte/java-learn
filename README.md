# java-learn

java基础

1、gitlab上创建新项目（New Project），输入项目名，eclipse通过git克隆项目到本地；
2、import project导入项目到本地，项目右击configure-convert to maven project转成maven项目
3、eclipse里第一次创建Maven项目时，src/main/java与src/test/java目录都不会出现，这是因为eclipse里的一个默认配置。
这两个目录是真实存在的，只是隐藏了。这时候想要让这两个目录出现，就需要修改以下配置：
右击项目-properties-Java Build Path-更改JRE System Library。
选择Edit,这里默认的是Execution environment,修改成本地的jdk根目录（jdk/bin的上级目录。
完成以上操作一般隐藏的目录就会出现。
   还是没有：项目右击-Build Path-New Source Folder-输入src/main/java 或者 src/main/resources

4、添加单元测试的方式： 项目右键-Build Path-Add Libraries-JUnit
   或者在pom文件中引入
   <dependency>
		<groupId>junit</groupId>
		<artifactId>junit</artifactId>
		<version>4.10</version>
   </dependency>