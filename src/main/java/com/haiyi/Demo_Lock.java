package com.haiyi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import org.junit.Test;

/**
 * 锁
 * 
 * 多线程有4种实现方法
		
		继承Thread类，重写run方法
		实现Runnable接口，重写run方法，实现Runnable接口的实现类的实例对象作为Thread构造函数的target
		通过Callable和FutureTask创建线程
		通过线程池创建线程
 * 
 * 同步的实现方面有两种，分别是synchronized,wait与notify
 * 
 * 
 * 2.wait():使一个线程处于等待状态，并且释放所持有的对象的lock。
 * sleep():使一个正在运行的线程处于睡眠状态，是一个静态方法， * 调用此方法要捕捉InterruptedException异常 * 。
 * 
 * notify():唤醒一个处于等待状态的线程，注意的是在调用此方法的时候，并不能确切的唤醒某一个等待状态的线程 ，而是由JVM确定唤醒哪个线程，而且不是按优先级  。　
 * 
 * notifyAll():唤醒所有处入等待状态的线程，注意并不是给所有唤醒线程一个对象的锁，而是让它们竞争。
 * 
 * 
 * @author Administrator
 *
 */
public class Demo_Lock extends Thread {
	
	/**
	 * 继承Thread类，重写run方法
	 */
    public void run() {
        // 编写自己的线程代码
        System.err.println(Thread.currentThread().getName());
    }
	
	@Test
	public void test1() {
		Demo_Lock threadDemo01 = new Demo_Lock(); 
        threadDemo01.setName("我是自定义的线程1");
        threadDemo01.start();       
        System.out.println(Thread.currentThread().toString());  
	}
	
	/**
	 * 实现Runnable接口，重写run方法，
	 */
	@Test
	public void test2() {
		System.out.println(Thread.currentThread().getName());
        Thread t1 = new Thread(new MyTask());
        t1.start(); 
	}
	
	/**
	 * 通过Callable和FutureTask创建线程
	 */
	@Test
	public void test3() {
		Callable<Object> oneCallable = new Tickets<Object>();
        FutureTask<Object> oneTask = new FutureTask<Object>(oneCallable);
        Thread t = new Thread(oneTask);
        System.out.println(Thread.currentThread().getName());
        t.start();
	}
	
	/**
	 * 
	 * 通过线程池创建线程
	 * 
	 */
	@Test
	public void test4() {
		// TODO Auto-generated method stub
        ExecutorService executorService = Executors.newFixedThreadPool(5);  
        for (int i = 0; i < 10; i++)  {  
        	MyTask thread = new MyTask();
            // Thread.sleep(1000);
            executorService.execute(thread);  
        }
        //关闭线程池
        executorService.shutdown();
	}

	/**
	 * 简写
	 */
	@Test
	public void test5() {
		List<Callable<Object>> task = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			task.add(() -> task());
		}
		List<Future<Object>> result = new ArrayList<>();
		
		try {
			result = Executors.newCachedThreadPool().invokeAll(task);
			for (Future<Object> f : result) {
				Object o = f.get();
				System.err.println("result: " + o);
			}
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public Integer task() {
		for (int i = 0; i < 10; i++) {
			System.err.println(Thread.currentThread().getName() + ": " + i);
		}
		return 1;
	}
}

/**
 * 
 * @author Administrator
 *
 */
class MyTask implements Runnable {
	
    @Override
    public void run() {
        // TODO Auto-generated method stub
        System.out.println(Thread.currentThread().getName()+"-->我是通过实现接口的线程实现方式！");
    }   
}

/**
 * 
 * @author Administrator
 *
 * @param <Object>
 */
class Tickets<Object> implements Callable<Object>{
 
    // 重写call方法
    @Override
    public Object call() throws Exception {
        // TODO Auto-generated method stub
        System.out.println(Thread.currentThread().getName()+"-->我是通过实现Callable接口通过FutureTask包装器来实现的线程");
        return null;
    }   
}
